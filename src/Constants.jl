# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2021 Indiscale GmbH <info@indiscale.com>
# Copyright (C) 2021 Florian Spreckelsen <f.spreckelsen@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with this program. If not, see
# <https://www.gnu.org/licenses/>.
#
# ** end header
#

module Constants

# exports from this module
export COLLECTION, DATATYPE, IMPORTANCE, MIN_CCAOSDB_VERSION, ROLE

# exports from the enum submodules
export BOOLEAN, DATETIME, DOUBLE, INTEGER, TEXT

export LIST

export FILE, PROPERTY, RECORD, RECORD_TYPE

export FIX, OBLIGATORY, RECOMMENDED, SUGGESTED

"""
The minimum version of CaosDB's cpplib and C interface that is
supported by this version of CaosDB.jl.
"""
const MIN_CCAOSDB_VERSION = v"0.0.17"

# enums have to be encapsulated in modules to prevent namespce conflicts, see
# https://bleepcoder.com/julia/413856244/feature-request-getproperty-on-enum-type-to-access-instances
module DATATYPE

export BOOLEAN, DATETIME, DOUBLE, INTEGER, TEXT

@enum _DATATYPE begin
    UNSPECIFIED
    BOOLEAN
    INTEGER
    DOUBLE
    DATETIME
    TEXT
end
end

module COLLECTION

export LIST

@enum _COLLECTION begin
    LIST
end
end

module ROLE

export FILE, PROPERTY, RECORD, RECORD_TYPE

@enum _ROLE begin
    UNSPECIFIED
    RECORD
    RECORD_TYPE
    PROPERTY
    FILE
end
end

module IMPORTANCE

export FIX, OBLIGATORY, RECOMMENDED, SUGGESTED

@enum _IMPORTANCE begin
    UNSPECIFIED
    OBLIGATORY
    RECOMMENDED
    SUGGESTED
    FIX
end
end

using .COLLECTION

using .DATATYPE

using .IMPORTANCE

using .ROLE

end
