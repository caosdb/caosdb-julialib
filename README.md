# caosdb-julialib

[![pipeline
status](https://gitlab.indiscale.com/caosdb/src/caosdb-julialib/badges/main/pipeline.svg)](https://gitlab.indiscale.com/caosdb/src/caosdb-julialib/-/commits/main)
[![coverage report](https://gitlab.indiscale.com/caosdb/src/caosdb-julialib/badges/main/coverage.svg)](https://gitlab.indiscale.com/caosdb/src/caosdb-julialib/-/commits/main)
[![documentation (placeholder)](https://img.shields.io/badge/docs-latest-blue.svg)](https://docs.indiscale.com/caosdb-julialib/)

## Welcome

This is the development repository of **LinkAhead Julia Client Library** - a 
client library for LinkAhead and a part of the LinkAhead project.

## Setup

Please read the [README\_SETUP.md](README_SETUP.md) for instructions
on how to develop, build, and use this code. caosdb-julialib is
developped and tested using Julia 1.6, we cannot guarantee its
compatibility with versions <= 1.5.

## Further reading

Please refer to the [official
documentation](https://docs.indiscale.com/caosdb-julialib/) for more
information.

## Contributing

Thank you very much to all contributers—[past,
present](https://gitlab.com/linkahead/linkahead/-/blob/main/HUMANS.md), and prospective
ones.

### Code of Conduct

By participating, you are expected to uphold our [Code of
Conduct](https://gitlab.com/linkahead/linkahead/-/blob/main/CODE_OF_CONDUCT.md).

### How to Contribute

* You found a bug, have a question, or want to request a feature? Please 
[create an issue](https://gitlab.com/linkahead/linkahead-julialib/-/issues).
* You want to contribute code?
    * **Forking:** Please fork the repository and create a merge request in GitLab and choose this repository as
      target. Make sure to select "Allow commits from members who can merge the target branch" under
      Contribution when creating the merge request. This allows our team to work with you on your
      request.
    * **Code style:** This project adhers to the PEP8 recommendations, you can test your code style
      using the `autopep8` tool (`autopep8 -i -r ./`).  Please write your doc strings following the
      [NumpyDoc](https://numpydoc.readthedocs.io/en/latest/format.html) conventions.
* You can also  join the LinkAhead community on
  [#linkahead:matrix.org](https://matrix.to/#/!unwwlTfOznjEnMMXxf:matrix.org).

## License

* Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>

All files in this repository are licensed under a [GNU Affero General
Public License](LICENSE.md) (version 3 or later).
