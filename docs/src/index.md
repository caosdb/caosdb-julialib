# CaosDB.jl documentation

Welcome to CaosDB.jl, the Julia Client for
[CaosDB](https://caosdb.org), the opensource research data management
system. In here you find instruction on how to [Set-up
CaosDB.jl](@ref), some introductory [Examples](@ref), and an overview
of [CaosDB.jl's API](@ref).

## Contents

```@contents
Pages = ["README_SETUP.md", "examples.md", "api.md"]
```
