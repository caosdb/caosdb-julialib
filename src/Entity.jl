# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2021 Indiscale GmbH <info@indiscale.com>
# Copyright (C) 2021 Florian Spreckelsen <f.spreckelsen@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with this program. If not, see
# <https://www.gnu.org/licenses/>.
#
# ** end header
#
module Entity

# Creators
export create_entity,
    create_parent,
    create_property,
    create_property_entity,
    create_record,
    create_recordtype,
    create_file_entity

# getters
export get_id,
    get_role,
    get_name,
    get_description,
    get_datatype,
    get_unit,
    get_value,
    get_version_id,
    get_property,
    get_properties,
    get_parent,
    get_parents,
    get_error,
    get_errors,
    get_warning,
    get_warnings,
    get_info,
    get_infos,
    get_importance,
    get_code

# setters
export append_parent,
    append_parents,
    append_property,
    append_properties,
    remove_parent,
    remove_property,
    set_id,
    set_role,
    set_name,
    set_description,
    set_datatype,
    set_unit,
    set_value,
    set_importance,
    set_local_path,
    set_remote_path

# helper functions
export has_errors, has_warnings

using CaosDB

# TODO(henrik, daniel) How much do we want to help the users when
# setting/getting the values? Should it only be allowed if the
# datatype of the entity/property is set? This might be too strong,
# especially in case of a property which I might want to specify by
# name, add a value, append it to a Record, and then insert it and
# might expect the server to care for the datatype.
"""
Struct containing a pointer to the wrapped cpp entity
object. Meant for internal use; use `CaosDB.Entity.create_entity` to
create an entity.
"""
mutable struct _Entity
    wrapped_entity::Ptr{Cvoid}
    _deletable::Bool

    function _Entity(managed_by_julia::Bool = false)

        entity = new()
        entity._deletable = false

        if managed_by_julia
            function f(t)
                ccall(
                    (:caosdb_entity_delete_entity, CaosDB.library_name),
                    Cint,
                    (Ref{_Entity},),
                    Ref{_Entity}(t),
                )
            end
            finalizer(f, entity)
        end

        return entity
    end
end


"""
Struct containing a pointer to the wrapped cpp parent
object. Meant for internal use; use `CaosDB.Entity.create_parent` to
create an parent.
"""
mutable struct _Parent
    wrapped_parent::Ptr{Cvoid}
    _deletable::Bool

    function _Parent(managed_by_julia::Bool = false)

        parent = new()
        parent._deletable = false

        if managed_by_julia
            function f(t)
                ccall(
                    (:caosdb_entity_delete_parent, CaosDB.library_name),
                    Cint,
                    (Ref{_Parent},),
                    Ref{_Parent}(t),
                )
            end
            finalizer(f, parent)
        end

        return parent
    end
end


"""
Struct containing a pointer to the wrapped cpp property
object. Meant for internal use; use `CaosDB.Entity.create_property` to
create an property.
"""
mutable struct _Property
    wrapped_property::Ptr{Cvoid}
    _deletable::Bool

    function _Property(managed_by_julia::Bool = false)

        property = new()
        property._deletable = false

        if managed_by_julia
            function f(t)
                ccall(
                    (:caosdb_entity_delete_property, CaosDB.library_name),
                    Cint,
                    (Ref{_Property},),
                    Ref{_Property}(t),
                )
            end
            finalizer(f, property)
        end

        return property
    end
end

"""
Struct containing a pointer to the wrapped cpp messsage object. Meant
for internal use only; don't create it by yourself.
"""
mutable struct _Message
    wrapped_message::Ptr{Cvoid}
    _deletable::Bool

    function _Message()
        mess = new()
        mess._deletable = false
        return mess
    end
end

"""
Struct containing a pointer to the wrapped cpp DataType object. Meant for
internal use only; use `CaosDB.Entity.create_<type>_datatype` to create a
valid _DataType object or use the `set/get_datatype` functions.
"""
mutable struct _DataType
    wrapped_datatype::Ptr{Cvoid}
    _deletable::Bool

    function _DataType(managed_by_julia::Bool = false)

        datatype = new()
        datatype._deletable = false

        if managed_by_julia
            function f(t)
                ccall(
                    (:caosdb_entity_delete_datatype, CaosDB.library_name),
                    Cint,
                    (Ref{_DataType},),
                    Ref{_DataType}(t),
                )
            end
            finalizer(f, datatype)
        end

        return datatype
    end
end

"""
Struct containing a pointer to the wrapped cpp AbstractValue object. Meant for
internal use only; use `CaosDB.Entity.create_value` to create a _Value object or
use the `set/get_value` functions`.
"""
mutable struct _Value
    wrapped_value::Ptr{Cvoid}
    _deletable::Bool

    function _Value(managed_by_julia::Bool = false)

        value = new()
        value._deletable = false

        if managed_by_julia
            function f(t)
                ccall(
                    (:caosdb_entity_delete_value, CaosDB.library_name),
                    Cint,
                    (Ref{_Value},),
                    Ref{_Value}(t),
                )
            end
            finalizer(f, value)
        end

        return value
    end
end

"""
    function create_entity(name::AbstractString = "")

Return a new entity object. If a `name` was provided, set the name of
the entity correspondingly.
"""
function create_entity(name::AbstractString = "")

    entity = Ref{_Entity}(_Entity(true))

    err_code = ccall(
        (:caosdb_entity_create_entity, CaosDB.library_name),
        Cint,
        (Ref{_Entity},),
        entity,
    )

    CaosDB.Exceptions.evaluate_return_code(err_code)

    if name != ""
        set_name(entity, name)
    end

    return entity

end

"""
    function create_recordtype(name::AbstractString = "")

Return a new entity object with role RecordType. If a `name` was
provided, its name is set accordingly.
"""
function create_recordtype(name::AbstractString = "")

    record_type = create_entity(name)

    set_role(record_type, CaosDB.Constants.ROLE.RECORD_TYPE)

    return record_type

end

"""
     function create_record(name::AbstractString = "")

Return a new entity object with role Record. If a `name` was provided,
its name is set accordingly.
"""
function create_record(name::AbstractString = "")

    record = create_entity(name)

    set_role(record, CaosDB.Constants.ROLE.RECORD)

    return record

end

"""
    function create_property_entity(;
        name::AbstractString = "",
        datatype::Union{AbstractString,CaosDB.Constants.DATATYPE._DATATYPE} = "",
        collection::Union{CaosDB.Constants.COLLECTION._COLLECTION,Nothing} = nothing,
        unit::AbstractString = "",
    )

Return a new entity object with role Record. If `name`, `datatype`, or `unit`
were provided, its name, datatype (including whether its `collection` type), or
unit are set accordingly.
"""
function create_property_entity(;
    name::AbstractString = "",
    datatype::Union{AbstractString,CaosDB.Constants.DATATYPE._DATATYPE} = "",
    collection::Union{CaosDB.Constants.COLLECTION._COLLECTION,Nothing} = nothing,
    unit::AbstractString = "",
)

    property = create_entity(name)

    set_role(property, CaosDB.Constants.ROLE.PROPERTY)

    if datatype != ""
        set_datatype(property, datatype, collection)
    end

    if unit != ""
        set_unit(property, unit)
    end

    return property
end

"""
    function create_property(;
        name::AbstractString = "",
        id::AbstractString = "",
        value::Union{AbstractString, Number, Bool} = "",
        datatype::Union{AbstractString,CaosDB.Constants.DATATYPE._DATATYPE} = "",
        collection::Union{CaosDB.Constants.COLLECTION._COLLECTION, Nothing} = nothing,
     )

Create a property object that can be appended to another `_Entity`. If `name`,
`id`, or `value` are given, the property's name, id, or value are set
accordingly. The datatype and its collection type can be specified with
`datatype` and `collection`, respectively.

!!! info

    This is not an `_Entity` that could be inserted or updated by its
    own but a `_Property` that is to be appended to another `_Entity`.
"""
function create_property(;
    name::AbstractString = "",
    id::AbstractString = "",
    value::Union{AbstractString,Number,Bool} = "",
    datatype::Union{AbstractString,CaosDB.Constants.DATATYPE._DATATYPE} = "",
    collection::Union{CaosDB.Constants.COLLECTION._COLLECTION,Nothing} = nothing,
)
    property = Ref{_Property}(_Property(true))

    err_code = ccall(
        (:caosdb_entity_create_property, CaosDB.library_name),
        Cint,
        (Ref{_Property},),
        property,
    )

    CaosDB.Exceptions.evaluate_return_code(err_code)

    if name != ""
        set_name(property, name)
    end

    if id != ""
        set_id(property, id)
    end

    if datatype != ""
        set_datatype(property, datatype, collection)
    end

    if value != ""
        set_value(property, value)
    end

    return property

end
"""
function create_file_entity(;
    local_path::AbstractString,
    remote_path::AbstractString,
    name::AbstractString = "",
)

Return a new entity object with role File.
`local_path` is the path of the file on the local file system. An exception is
thrown if this file is not existent. `remote_path` is the path of the file on
the remote caosdb server.
"""
function create_file_entity(;
    local_path::AbstractString,
    remote_path::AbstractString,
    name::AbstractString = "",
)
    if isfile(local_path) == false
        throw(
            CaosDB.Exceptions.ClientException("Cannot find the local file '$local_path'."),
        )
    end

    file_entity = create_entity(name)
    set_role(file_entity, CaosDB.Constants.ROLE.FILE)
    set_local_path(file_entity, local_path)
    set_remote_path(file_entity, remote_path)

    return file_entity
end
"""
    function create_parent(; name::AbstractString = "", id::AbstractString = "")

Create a parent object that can be appended to another `_Entity`. If
`name`, `id`, or `value` are given, the parent's name, id, or value
are set accordingly.

!!! info

    This is not an `_Entity` that could be inserted or updated by its
    own but a `_Parent` that is to be appended to another `_Entity`.
"""
function create_parent(; name::AbstractString = "", id::AbstractString = "")

    parent = Ref{_Parent}(_Parent(true))

    err_code = ccall(
        (:caosdb_entity_create_parent, CaosDB.library_name),
        Cint,
        (Ref{_Parent},),
        parent,
    )

    CaosDB.Exceptions.evaluate_return_code(err_code)

    if name != ""
        set_name(parent, name)
    end

    if id != ""
        set_id(parent, id)
    end

    return parent
end

"""
    function create_atomic_datatype(name::CaosDB.Constants.DATATYPE._DATATYPE)

Create and return a DataType object with an atomic datatype specified by the
`name`.
"""
function create_atomic_datatype(name::CaosDB.Constants.DATATYPE._DATATYPE)

    datatype = Ref{_DataType}(_DataType(true))

    err_code = ccall(
        (:caosdb_entity_create_atomic_datatype, CaosDB.library_name),
        Cint,
        (Ref{_DataType}, Cstring),
        datatype,
        string(name),
    )

    CaosDB.Exceptions.evaluate_return_code(err_code)

    return datatype
end

"""
    function create_reference_datatype(name::AbstractString)

Create and return a DataType object with a reference datatype specified by
`name`.
"""
function create_reference_datatype(name::AbstractString)

    datatype = Ref{_DataType}(_DataType(true))

    err_code = ccall(
        (:caosdb_entity_create_reference_datatype, CaosDB.library_name),
        Cint,
        (Ref{_DataType}, Cstring),
        datatype,
        name,
    )

    CaosDB.Exceptions.evaluate_return_code(err_code)

    return datatype
end

"""
    function create_atomic_list_datatype(name::CaosDB.Constants.DATATYPE._DATATYPE)

Create and return a DataType object which is a list of atomics specified by
`name`.
"""
function create_atomic_list_datatype(name::CaosDB.Constants.DATATYPE._DATATYPE)

    datatype = Ref{_DataType}(_DataType(true))

    err_code = ccall(
        (:caosdb_entity_create_atomic_list_datatype, CaosDB.library_name),
        Cint,
        (Ref{_DataType}, Cstring),
        datatype,
        string(name),
    )

    CaosDB.Exceptions.evaluate_return_code(err_code)

    return datatype
end

"""
    function create_reference_list_datatype(name::AbstractString)

Create and return a DataType object which is a list of references specified by
`name`.
"""
function create_reference_list_datatype(name::AbstractString)

    datatype = Ref{_DataType}(_DataType(true))

    err_code = ccall(
        (:caosdb_entity_create_reference_list_datatype, CaosDB.library_name),
        Cint,
        (Ref{_DataType}, Cstring),
        datatype,
        name,
    )

    CaosDB.Exceptions.evaluate_return_code(err_code)

    return datatype
end

function create_value(
    value::Union{AbstractString,Number,Bool,Vector{T}},
) where {T<:Union{AbstractString,Number,Bool}}

    in_type = typeof(value)
    out = Ref{_Value}(_Value(true))
    if in_type <: AbstractString
        err_code = ccall(
            (:caosdb_entity_create_string_value, CaosDB.library_name),
            Cint,
            (Ref{_Value}, Cstring),
            out,
            value,
        )
    elseif in_type <: Bool
        err_code = ccall(
            (:caosdb_entity_create_bool_value, CaosDB.library_name),
            Cint,
            (Ref{_Value}, Bool),
            out,
            value,
        )
    elseif in_type <: Integer
        err_code = ccall(
            (:caosdb_entity_create_int_value, CaosDB.library_name),
            Cint,
            (Ref{_Value}, Clong),
            out,
            Clong(value),
        )
    elseif in_type <: Number
        err_code = ccall(
            (:caosdb_entity_create_double_value, CaosDB.library_name),
            Cint,
            (Ref{_Value}, Cdouble),
            out,
            Cdouble(value),
        )
    elseif in_type <: Vector{T} where {T<:AbstractString}
        err_code = ccall(
            (:caosdb_entity_create_string_vector_value, CaosDB.library_name),
            Cint,
            (Ref{_Value}, Ptr{Ptr{Cchar}}, Cint),
            out,
            value,
            Cint(length(value)),
        )
    elseif in_type <: Vector{T} where {T<:Bool}
        err_code = ccall(
            (:caosdb_entity_create_bool_vector_value, CaosDB.library_name),
            Cint,
            (Ref{_Value}, Ptr{Bool}, Cint),
            out,
            value,
            Cint(length(value)),
        )
    elseif in_type <: Vector{T} where {T<:Integer}
        err_code = ccall(
            (:caosdb_entity_create_int_vector_value, CaosDB.library_name),
            Cint,
            (Ref{_Value}, Ptr{Clong}, Cint),
            out,
            Vector{Clong}(value),
            Cint(length(value)),
        )
    elseif in_type <: Vector{T} where {T<:Number}
        err_code = ccall(
            (:caosdb_entity_create_double_vector_value, CaosDB.library_name),
            Cint,
            (Ref{_Value}, Ptr{Cdouble}, Cint),
            out,
            Vector{Cdouble}(value),
            Cint(length(value)),
        )
    else
        # Should never enter here but treat it just in case
        throw(
            ArgumentError(
                "The argument of type $in_type couldn't be converted into a valid CaosDB value object.",
            ),
        )
    end

    CaosDB.Exceptions.evaluate_return_code(err_code)

    return out
end

"""
    function get_id(entity::Ref{_Entity})

Return the id of the given `entity`
"""
function get_id(entity::Ref{_Entity})

    out = Ref{Ptr{UInt8}}(Ptr{UInt8}())

    err_code = ccall(
        (:caosdb_entity_entity_get_id, CaosDB.library_name),
        Cint,
        (Ref{_Entity}, Ref{Ptr{UInt8}}),
        entity,
        out,
    )

    CaosDB.Exceptions.evaluate_return_code(err_code)

    return unsafe_string(out[])
end

"""
    function get_id(entity::Ref{_Parent})

Return the id of the given `parent`
"""
function get_id(parent::Ref{_Parent})

    out = Ref{Ptr{UInt8}}(Ptr{UInt8}())

    err_code = ccall(
        (:caosdb_entity_parent_get_id, CaosDB.library_name),
        Cint,
        (Ref{_Parent}, Ref{Ptr{UInt8}}),
        parent,
        out,
    )

    CaosDB.Exceptions.evaluate_return_code(err_code)

    return unsafe_string(out[])
end

"""
    function get_id(entity::Ref{_Property})

Return the id of the given `property`
"""
function get_id(property::Ref{_Property})

    out = Ref{Ptr{UInt8}}(Ptr{UInt8}())

    err_code = ccall(
        (:caosdb_entity_property_get_id, CaosDB.library_name),
        Cint,
        (Ref{_Property}, Ref{Ptr{UInt8}}),
        property,
        out,
    )

    CaosDB.Exceptions.evaluate_return_code(err_code)

    return unsafe_string(out[])
end

"""
    function get_role(entity::Ref{_Entity})

Return the role of the given `entity`.
"""
function get_role(entity::Ref{_Entity})

    out = Ref{Ptr{UInt8}}(Ptr{UInt8}())

    err_code = ccall(
        (:caosdb_entity_entity_get_role, CaosDB.library_name),
        Cint,
        (Ref{_Entity}, Ref{Ptr{UInt8}}),
        entity,
        out,
    )

    CaosDB.Exceptions.evaluate_return_code(err_code)

    rolename = unsafe_string(out[])
    role = getproperty(CaosDB.Constants.ROLE, Symbol(rolename))
    return role
end

"""
    function get_name(entity::Ref{_Entity})

Return the name of the given `entity`
"""
function get_name(entity::Ref{_Entity})

    out = Ref{Ptr{UInt8}}(Ptr{UInt8}())

    err_code = ccall(
        (:caosdb_entity_entity_get_name, CaosDB.library_name),
        Cint,
        (Ref{_Entity}, Ref{Ptr{UInt8}}),
        entity,
        out,
    )

    CaosDB.Exceptions.evaluate_return_code(err_code)

    return unsafe_string(out[])
end

"""
    function get_name(entity::Ref{_Parent})

Return the name of the given `parent`
"""
function get_name(parent::Ref{_Parent})

    out = Ref{Ptr{UInt8}}(Ptr{UInt8}())

    err_code = ccall(
        (:caosdb_entity_parent_get_name, CaosDB.library_name),
        Cint,
        (Ref{_Parent}, Ref{Ptr{UInt8}}),
        parent,
        out,
    )

    CaosDB.Exceptions.evaluate_return_code(err_code)

    return unsafe_string(out[])
end

"""
    function get_name(entity::Ref{_Property})

Return the name of the given `property`
"""
function get_name(property::Ref{_Property})

    out = Ref{Ptr{UInt8}}(Ptr{UInt8}())

    err_code = ccall(
        (:caosdb_entity_property_get_name, CaosDB.library_name),
        Cint,
        (Ref{_Property}, Ref{Ptr{UInt8}}),
        property,
        out,
    )

    CaosDB.Exceptions.evaluate_return_code(err_code)

    return unsafe_string(out[])
end

"""
    function get_description(entity::Ref{_Entity})

Return the description of the given `entity`
"""
function get_description(entity::Ref{_Entity})

    out = Ref{Ptr{UInt8}}(Ptr{UInt8}())

    err_code = ccall(
        (:caosdb_entity_entity_get_description, CaosDB.library_name),
        Cint,
        (Ref{_Entity}, Ref{Ptr{UInt8}}),
        entity,
        out,
    )

    CaosDB.Exceptions.evaluate_return_code(err_code)

    return unsafe_string(out[])
end

"""
    function get_description(entity::Ref{_Parent})

Return the description of the given `parent`
"""
function get_description(parent::Ref{_Parent})

    out = Ref{Ptr{UInt8}}(Ptr{UInt8}())

    err_code = ccall(
        (:caosdb_entity_parent_get_description, CaosDB.library_name),
        Cint,
        (Ref{_Parent}, Ref{Ptr{UInt8}}),
        parent,
        out,
    )

    CaosDB.Exceptions.evaluate_return_code(err_code)

    return unsafe_string(out[])
end

"""
    function get_description(entity::Ref{_Property})

Return the description of the given `property`
"""
function get_description(property::Ref{_Property})

    out = Ref{Ptr{UInt8}}(Ptr{UInt8}())

    err_code = ccall(
        (:caosdb_entity_property_get_description, CaosDB.library_name),
        Cint,
        (Ref{_Property}, Ref{Ptr{UInt8}}),
        property,
        out,
    )

    CaosDB.Exceptions.evaluate_return_code(err_code)

    return unsafe_string(out[])
end

"""
    function get_description(entity::Ref{_Message})

Return the description of the given `message`
"""
function get_description(message::Ref{_Message})

    out = Ref{Ptr{UInt8}}(Ptr{UInt8}())

    err_code = ccall(
        (:caosdb_entity_message_get_description, CaosDB.library_name),
        Cint,
        (Ref{_Message}, Ref{Ptr{UInt8}}),
        message,
        out,
    )

    CaosDB.Exceptions.evaluate_return_code(err_code)

    return unsafe_string(out[])
end

"""
    function _get_datatype(datatype::Ref{_DataType})

Return a tuple that contains the name of the `datatype`, and its collection type
(`nothing` in case of a scalar datatype).
"""
function _get_datatype(datatype::Ref{_DataType})

    name = Ref{Ptr{UInt8}}(Ptr{UInt8}())
    err_code = ccall(
        (:caosdb_entity_datatype_get_datatype_name, CaosDB.library_name),
        Cint,
        (Ref{_DataType}, Ref{Ptr{UInt8}}),
        datatype,
        name,
    )
    CaosDB.Exceptions.evaluate_return_code(err_code)
    name = unsafe_string(name[])

    is_a = Ref{Bool}(false)
    err_code = ccall(
        (:caosdb_entity_datatype_is_atomic, CaosDB.library_name),
        Cint,
        (Ref{_DataType}, Ref{Bool}),
        datatype,
        is_a,
    )
    CaosDB.Exceptions.evaluate_return_code(err_code)
    if is_a[]
        return getproperty(CaosDB.Constants.DATATYPE, Symbol(name)), nothing
    end

    err_code = ccall(
        (:caosdb_entity_datatype_is_reference, CaosDB.library_name),
        Cint,
        (Ref{_DataType}, Ref{Bool}),
        datatype,
        is_a,
    )
    CaosDB.Exceptions.evaluate_return_code(err_code)
    if is_a[]
        return name, nothing
    end

    err_code = ccall(
        (:caosdb_entity_datatype_is_list_of_atomic, CaosDB.library_name),
        Cint,
        (Ref{_DataType}, Ref{Bool}),
        datatype,
        is_a,
    )
    CaosDB.Exceptions.evaluate_return_code(err_code)
    if is_a[]
        return getproperty(CaosDB.Constants.DATATYPE, Symbol(name)),
        CaosDB.Constants.COLLECTION.LIST
    end

    err_code = ccall(
        (:caosdb_entity_datatype_is_list_of_reference, CaosDB.library_name),
        Cint,
        (Ref{_DataType}, Ref{Bool}),
        datatype,
        is_a,
    )
    CaosDB.Exceptions.evaluate_return_code(err_code)
    if is_a[]
        return name, CaosDB.Constants.COLLECTION.LIST
    end

    throw(CaosDB.Exceptions.ClientException("Unkown datatype"))

end

"""
    function get_datatype(entity::Ref{_Entity})

Return a tuple that contains the name of the datatype of the given `entity`, and
its collection type (`nothing` in case of a scalar datatype).
"""
function get_datatype(entity::Ref{_Entity})

    out = Ref{_DataType}(_DataType())
    err_code = ccall(
        (:caosdb_entity_entity_get_datatype, CaosDB.library_name),
        Cint,
        (Ref{_Entity}, Ref{_DataType}),
        entity,
        out,
    )

    CaosDB.Exceptions.evaluate_return_code(err_code)

    return _get_datatype(out)
end

"""
    function get_datatype(entity::Ref{_Property})

Return a tuple that contains the name of the datatype of the given
`property`, whether it is a reference, and whether it is a list.
"""
function get_datatype(property::Ref{_Property})

    out = Ref{_DataType}(_DataType())
    err_code = ccall(
        (:caosdb_entity_property_get_datatype, CaosDB.library_name),
        Cint,
        (Ref{_Property}, Ref{_DataType}),
        property,
        out,
    )

    CaosDB.Exceptions.evaluate_return_code(err_code)

    return _get_datatype(out)
end

"""
    function get_unit(entity::Ref{_Entity})

Return the unit of the given `entity`
"""
function get_unit(entity::Ref{_Entity})

    out = Ref{Ptr{UInt8}}(Ptr{UInt8}())

    err_code = ccall(
        (:caosdb_entity_entity_get_unit, CaosDB.library_name),
        Cint,
        (Ref{_Entity}, Ref{Ptr{UInt8}}),
        entity,
        out,
    )

    CaosDB.Exceptions.evaluate_return_code(err_code)

    return unsafe_string(out[])
end

"""
    function get_unit(entity::Ref{_Property})

Return the unit of the given `property`
"""
function get_unit(property::Ref{_Property})

    out = Ref{Ptr{UInt8}}(Ptr{UInt8}())

    err_code = ccall(
        (:caosdb_entity_property_get_unit, CaosDB.library_name),
        Cint,
        (Ref{_Property}, Ref{Ptr{UInt8}}),
        property,
        out,
    )

    CaosDB.Exceptions.evaluate_return_code(err_code)

    return unsafe_string(out[])
end

"""
    function _get_value(value::Ref{_Value})

Return the value of the given CaosDB value object.
"""
function _get_value(value::Ref{_Value})

    is_a = Ref{Bool}(false)

    # value may be null
    err_code = ccall(
        (:caosdb_entity_value_is_null, CaosDB.library_name),
        Cint,
        (Ref{_Value}, Ref{Bool}),
        value,
        is_a,
    )

    CaosDB.Exceptions.evaluate_return_code(err_code)

    if is_a[]
        return nothing
    end

    # scalar string
    err_code = ccall(
        (:caosdb_entity_value_is_string, CaosDB.library_name),
        Cint,
        (Ref{_Value}, Ref{Bool}),
        value,
        is_a,
    )

    CaosDB.Exceptions.evaluate_return_code(err_code)

    if is_a[]
        out = Ref{Ptr{UInt8}}(Ptr{UInt8}())

        ccall(
            (:caosdb_entity_value_get_as_string, CaosDB.library_name),
            Cint,
            (Ref{_Value}, Ref{Ptr{UInt8}}),
            value,
            out,
        )

        CaosDB.Exceptions.evaluate_return_code(err_code)

        return unsafe_string(out[])
    end

    # integer
    err_code = ccall(
        (:caosdb_entity_value_is_integer, CaosDB.library_name),
        Cint,
        (Ref{_Value}, Ref{Bool}),
        value,
        is_a,
    )

    CaosDB.Exceptions.evaluate_return_code(err_code)

    if is_a[]
        out = Ref{Clong}(0)

        ccall(
            (:caosdb_entity_value_get_as_integer, CaosDB.library_name),
            Cint,
            (Ref{_Value}, Ref{Clong}),
            value,
            out,
        )

        CaosDB.Exceptions.evaluate_return_code(err_code)

        return out[]
    end

    # double
    err_code = ccall(
        (:caosdb_entity_value_is_double, CaosDB.library_name),
        Cint,
        (Ref{_Value}, Ref{Bool}),
        value,
        is_a,
    )

    CaosDB.Exceptions.evaluate_return_code(err_code)

    if is_a[]
        out = Ref{Cdouble}(0)

        ccall(
            (:caosdb_entity_value_get_as_double, CaosDB.library_name),
            Cint,
            (Ref{_Value}, Ref{Cdouble}),
            value,
            out,
        )

        CaosDB.Exceptions.evaluate_return_code(err_code)

        return out[]
    end

    # bool
    err_code = ccall(
        (:caosdb_entity_value_is_bool, CaosDB.library_name),
        Cint,
        (Ref{_Value}, Ref{Bool}),
        value,
        is_a,
    )

    CaosDB.Exceptions.evaluate_return_code(err_code)

    if is_a[]
        out = Ref{Bool}(false)

        ccall(
            (:caosdb_entity_value_get_as_bool, CaosDB.library_name),
            Cint,
            (Ref{_Value}, Ref{Bool}),
            value,
            out,
        )

        CaosDB.Exceptions.evaluate_return_code(err_code)

        return convert(Bool, out[])
    end

    # vector
    err_code = ccall(
        (:caosdb_entity_value_is_vector, CaosDB.library_name),
        Cint,
        (Ref{_Value}, Ref{Bool}),
        value,
        is_a,
    )

    CaosDB.Exceptions.evaluate_return_code(err_code)

    if is_a[]
        vector_size = Ref{Cint}(0)

        err_code = ccall(
            (:caosdb_entity_value_get_as_vector_size, CaosDB.library_name),
            Cint,
            (Ref{_Value}, Ref{Cint}),
            value,
            vector_size,
        )

        CaosDB.Exceptions.evaluate_return_code(err_code)

        out = []
        value_elt = Ref{_Value}(_Value(false))
        for ii::Cint = 1:vector_size[]
            err_code = ccall(
                (:caosdb_entity_value_get_as_vector_at, CaosDB.library_name),
                Cint,
                (Ref{_Value}, Ref{_Value}, Cint),
                value,
                value_elt,
                ii - Cint(1),
            )
            CaosDB.Exceptions.evaluate_return_code(err_code)
            push!(out, _get_value(value_elt))
        end

        if length(out) > 0
            # convert to vector of type of elements s.th. it can be re-used in a
            # `set_value` function.
            elt_type = typeof(out[1])
            return convert(Vector{elt_type}, out)
        end

        return out
    end

    throw(CaosDB.Exceptions.ClientException("Unkown value."))
end

"""
    function get_value(entity::Ref{_Entity})

Return the value of the given `entity`
"""
function get_value(entity::Ref{_Entity})

    value = Ref{_Value}(_Value())

    err_code = ccall(
        (:caosdb_entity_entity_get_value, CaosDB.library_name),
        Cint,
        (Ref{_Entity}, Ref{_Value}),
        entity,
        value,
    )
    CaosDB.Exceptions.evaluate_return_code(err_code)

    return _get_value(value)
end

"""
    function get_value(property::Ref{_Property})

Return the value of the given `property`
"""
function get_value(property::Ref{_Property})

    value = Ref{_Value}(_Value())

    err_code = ccall(
        (:caosdb_entity_property_get_value, CaosDB.library_name),
        Cint,
        (Ref{_Property}, Ref{_Value}),
        property,
        value,
    )
    CaosDB.Exceptions.evaluate_return_code(err_code)

    return _get_value(value)
end

"""
    function get_version_id(entity::Ref{_Entity})

Return the version_id of the given `entity`
"""
function get_version_id(entity::Ref{_Entity})

    out = Ref{Ptr{UInt8}}(Ptr{UInt8}())

    err_code = ccall(
        (:caosdb_entity_entity_get_version_id, CaosDB.library_name),
        Cint,
        (Ref{_Entity}, Ref{Ptr{UInt8}}),
        entity,
        out,
    )

    CaosDB.Exceptions.evaluate_return_code(err_code)

    return unsafe_string(out[])
end

"""
    function get_code(message::Ref{_Message})

Return the code of the given `message`.
"""
function get_code(message::Ref{_Message})

    code = Ref{Cint}(0)

    err_code = ccall(
        (:caosdb_entity_message_get_code, CaosDB.library_name),
        Cint,
        (Ref{_Message}, Ref{Cint}),
        message,
        code,
    )

    CaosDB.Exceptions.evaluate_return_code(err_code)

    return code

end

"""
    function get_importance(property::Ref{_Property})

Return the importance of the given `property`
"""
function get_importance(property::Ref{_Property})

    out = Ref{Ptr{UInt8}}(Ptr{UInt8}())

    err_code = ccall(
        (:caosdb_entity_property_get_importance, CaosDB.library_name),
        Cint,
        (Ref{_Property}, Ref{Ptr{UInt8}}),
        property,
        out,
    )

    CaosDB.Exceptions.evaluate_return_code(err_code)

    imp_name = unsafe_string(out[])
    imp = getproperty(CaosDB.Constants.IMPORTANCE, Symbol(imp_name))
    return imp
end

"""
    function get_errors_size(entity::Ref{_Entity})

Return the number of error messages attached to the given `entity`.
"""
function get_errors_size(entity::Ref{_Entity})

    size = Ref{Cint}(0)

    err_code = ccall(
        (:caosdb_entity_entity_get_errors_size, CaosDB.library_name),
        Cint,
        (Ref{_Entity}, Ref{Cint}),
        entity,
        size,
    )

    CaosDB.Exceptions.evaluate_return_code(err_code)

    return size[]

end

"""
    function get_error(entity::Ref{_Entity}, index::Cint)

Return the error message of the given `entity` with the provided
`index`.
"""
function get_error(entity::Ref{_Entity}, index::Cint)

    size = get_errors_size(entity)

    if index > size
        throw(
            DomainError(
                index,
                "You tried to access the error at position $index but the entity only has $size errors.",
            ),
        )
    end

    error = Ref{_Message}(_Message())

    err_code = ccall(
        (:caosdb_entity_entity_get_error, CaosDB.library_name),
        Cint,
        (Ref{_Entity}, Cint),
        entity,
        index - Cint(1),
    )

    CaosDB.Exceptions.evaluate_return_code(err_code)

    message_text = get_description(error)

    code = get_code(error)

    return CaosDB.Exceptions.CaosDBMessage(message_text, code)
end

"""
    function get_error(entity::Ref{_Entity}, index::Integer)

Convenience wrapper for `get_error(::Ref{_Entity}, ::Cint)`. Convert
`index` to Int32 and return the error at position `index` of the given
`entity`.
"""
function get_error(entity::Ref{_Entity}, index::Integer)

    return get_error(entity, Cint(index))

end

"""
    function get_warnings_size(entity::Ref{_Entity})

Return the number of warning messages attached to the given `entity`.
"""
function get_warnings_size(entity::Ref{_Entity})

    size = Ref{Cint}(0)

    err_code = ccall(
        (:caosdb_entity_entity_get_warnings_size, CaosDB.library_name),
        Cint,
        (Ref{_Entity}, Ref{Cint}),
        entity,
        size,
    )

    CaosDB.Exceptions.evaluate_return_code(err_code)

    return size[]

end

"""
    function get_warning(entity::Ref{_Entity}, index::Cint)

Return the warning message of the given `entity` with the provided
`index`.
"""
function get_warning(entity::Ref{_Entity}, index::Cint)

    size = get_warnings_size(entity)

    if index > size
        throw(
            DomainError(
                index,
                "You tried to access the warning at position $index but the entity only has $size warnings.",
            ),
        )
    end

    warning = Ref{_Message}(_Message())

    err_code = ccall(
        (:caosdb_entity_entity_get_warning, CaosDB.library_name),
        Cint,
        (Ref{_Entity}, Cint),
        entity,
        index - Cint(1),
    )

    CaosDB.Exceptions.evaluate_return_code(err_code)

    message_text = get_description(warning)

    code = get_code(warning)

    return CaosDB.Exceptions.CaosDBMessage(message_text, code)
end

"""
    function get_warning(entity::Ref{_Entity}, index::Integer)

Convenience wrapper for `get_warning(::Ref{_Entity}, ::Cint)`. Convert
`index` to Int32 and return the warning at position `index` of the given
`entity`.
"""
function get_warning(entity::Ref{_Entity}, index::Integer)

    return get_warning(entity, Cint(index))

end

"""
    function get_infos_size(entity::Ref{_Entity})

Return the number of info messages attached to the given `entity`.
"""
function get_infos_size(entity::Ref{_Entity})

    size = Ref{Cint}(0)

    err_code = ccall(
        (:caosdb_entity_entity_get_infos_size, CaosDB.library_name),
        Cint,
        (Ref{_Entity}, Ref{Cint}),
        entity,
        size,
    )

    CaosDB.Exceptions.evaluate_return_code(err_code)

    return size[]

end

"""
    function get_info(entity::Ref{_Entity}, index::Cint)

Return the info message of the given `entity` with the provided
`index`.
"""
function get_info(entity::Ref{_Entity}, index::Cint)

    size = get_infos_size(entity)

    if index > size
        throw(
            DomainError(
                index,
                "You tried to access the info at position $index but the entity only has $size infos.",
            ),
        )
    end

    info = Ref{_Message}(_Message())

    err_code = ccall(
        (:caosdb_entity_entity_get_info, CaosDB.library_name),
        Cint,
        (Ref{_Entity}, Cint),
        entity,
        index - Cint(1),
    )

    CaosDB.Exceptions.evaluate_return_code(err_code)

    message_text = get_description(info)

    code = get_code(info)

    return CaosDB.Exceptions.CaosDBMessage(message_text, code)
end

"""
    function get_info(entity::Ref{_Entity}, index::Integer)

Convenience wrapper for `get_info(::Ref{_Entity}, ::Cint)`. Convert
`index` to Int32 and return the info at position `index` of the given
`entity`.
"""
function get_info(entity::Ref{_Entity}, index::Integer)

    return get_info(entity, Cint(index))

end


"""
    function get_errors(entity::Ref{_Entity})

Return a Vector of all error messages attached to the given `entity`.
"""
function get_errors(entity::Ref{_Entity})

    size = get_errors_size(entity)

    errors = [get_error(entity, Cint(ii)) for ii = 1:size]

    return errors
end

"""
    function get_warnings(entity::Ref{_Entity})

Return a Vector of all warning messages attached to the given `entity`.
"""
function get_warnings(entity::Ref{_Entity})

    size = get_warnings_size(entity)

    warnings = [get_warning(entity, Cint(ii)) for ii = 1:size]

    return warnings
end


"""
    function get_warnings(entity::Ref{_Entity})

Return a Vector of all info messages attached to the given `entity`.
"""
function get_infos(entity::Ref{_Entity})

    size = get_infos_size(entity)

    infos = [get_info(entity, Cint(ii)) for ii = 1:size]

    return infos
end

"""
    function get_parents_size(entity::Ref{_Entity})

Return the number of parents attached to the given `entity`.
"""
function get_parents_size(entity::Ref{_Entity})

    size = Ref{Cint}(0)

    err_code = ccall(
        (:caosdb_entity_entity_get_parents_size, CaosDB.library_name),
        Cint,
        (Ref{_Entity}, Ref{Cint}),
        entity,
        size,
    )

    CaosDB.Exceptions.evaluate_return_code(err_code)

    return size[]

end

"""
    function get_parent(entity::Ref{_Entity}, index::Cint)

Return the parent of the given `entity` at position `index`.
"""
function get_parent(entity::Ref{_Entity}, index::Cint)

    size = get_parents_size(entity)

    if index > size
        throw(
            DomainError(
                index,
                "You tried to access the parent at position $index but the entity only has $size parents.",
            ),
        )
    end

    parent = Ref{_Parent}(_Parent())

    err_code = ccall(
        (:caosdb_entity_entity_get_parent, CaosDB.library_name),
        Cint,
        (Ref{_Entity}, Ref{_Parent}, Cint),
        entity,
        parent,
        index - Cint(1),
    )

    CaosDB.Exceptions.evaluate_return_code(err_code)

    return parent
end

"""
    function get_parent(entity::Ref{_Entity}, index::Integer)

Convenience wrapper for `get_parent(::Ref{_Entity}, ::Cint)`. Convert
`index` to Int32 and return the parent at position `index` of the given
`entity`.
"""
function get_parent(entity::Ref{_Entity}, index::Integer)

    return get_parent(entity, Cint(index))

end


"""
    function get_parents(entity::Ref{_Entity})

Return the vector of all parents of the given `entity`.
"""
function get_parents(entity::Ref{_Entity})

    size = get_parents_size(entity)

    parents = [get_parent(entity, Cint(ii)) for ii = 1:size]

    return parents
end

"""
    function get_properties_size(entity::Ref{_Entity})

Return the number of properties attached to the given `entity`.
"""
function get_properties_size(entity::Ref{_Entity})

    size = Ref{Cint}(0)

    err_code = ccall(
        (:caosdb_entity_entity_get_properties_size, CaosDB.library_name),
        Cint,
        (Ref{_Entity}, Ref{Cint}),
        entity,
        size,
    )

    CaosDB.Exceptions.evaluate_return_code(err_code)

    return size[]

end

"""
    function get_property(entity::Ref{_Entity}, index::Cint)

Return the property of the given `entity` at position `index`.
"""
function get_property(entity::Ref{_Entity}, index::Cint)

    size = get_properties_size(entity)

    if index > size
        throw(
            DomainError(
                index,
                "You tried to access the property at position $index but the entity only has $size properties.",
            ),
        )
    end

    property = Ref{_Property}(_Property())

    err_code = ccall(
        (:caosdb_entity_entity_get_property, CaosDB.library_name),
        Cint,
        (Ref{_Entity}, Ref{_Property}, Cint),
        entity,
        property,
        index - Cint(1),
    )

    CaosDB.Exceptions.evaluate_return_code(err_code)

    return property
end


"""
    function get_property(entity::Ref{_Entity}, index::Integer)

Convenience wrapper for `get_property(::Ref{_Entity}, ::Cint)`. Convert
`index` to Int32 and return the property at position `index` of the given
`entity`.
"""
function get_property(entity::Ref{_Entity}, index::Integer)

    return get_property(entity, Cint(index))

end


"""
    function get_properties(entity::Ref{_Entity})

Return the vector of all properties of the given `entity`.
"""
function get_properties(entity::Ref{_Entity})

    size = get_properties_size(entity)

    properties = [get_property(entity, Cint(ii)) for ii = 1:size]

    return properties
end

"""
    function set_id(entity::Ref{_Entity}, id::AbstractString)

Throws a `CaosDB.Exceptions.ClientException` since you are not allowed
to set the id of entities.
"""
function set_id(entity::Ref{_Entity}, id::AbstractString)
    throw(
        CaosDB.Exceptions.ClientException(
            "You cannot set the id of an entity object.
If you want to update an entity with this id, you have to retrieve it first.",
        ),
    )
end

"""
    function set_id(parent::Ref{_Parent}, id::AbstractString)

Set the id of the given `parent` object.
"""
function set_id(parent::Ref{_Parent}, id::AbstractString)
    err_code = ccall(
        (:caosdb_entity_parent_set_id, CaosDB.library_name),
        Cint,
        (Ref{_Parent}, Cstring),
        parent,
        id,
    )

    CaosDB.Exceptions.evaluate_return_code(err_code)
end

"""
    function set_id(property::Ref{_Property}, id::AbstractString)

Set the id of the given `property` object.
"""
function set_id(property::Ref{_Property}, id::AbstractString)
    err_code = ccall(
        (:caosdb_entity_property_set_id, CaosDB.library_name),
        Cint,
        (Ref{_Property}, Cstring),
        property,
        id,
    )

    CaosDB.Exceptions.evaluate_return_code(err_code)
end

"""
    function set_role(entity::Ref{_Entity}, role::CaosDB.Constants.ROLE._ROLE)

Set the role of the given `entity` object.
"""
function set_role(entity::Ref{_Entity}, role::CaosDB.Constants.ROLE._ROLE)
    err_code = ccall(
        (:caosdb_entity_entity_set_role, CaosDB.library_name),
        Cint,
        (Ref{_Entity}, Cstring),
        entity,
        string(role),
    )

    CaosDB.Exceptions.evaluate_return_code(err_code)
end


"""
    function set_name(entity::Ref{_Entity}, name::AbstractString)

Set the name of the given `entity` object.
"""
function set_name(entity::Ref{_Entity}, name::AbstractString)
    err_code = ccall(
        (:caosdb_entity_entity_set_name, CaosDB.library_name),
        Cint,
        (Ref{_Entity}, Cstring),
        entity,
        name,
    )

    CaosDB.Exceptions.evaluate_return_code(err_code)
end


"""
    function set_name(property::Ref{_Property}, name::AbstractString)

Set the name of the given `property` object.
"""
function set_name(property::Ref{_Property}, name::AbstractString)
    err_code = ccall(
        (:caosdb_entity_property_set_name, CaosDB.library_name),
        Cint,
        (Ref{_Property}, Cstring),
        property,
        name,
    )

    CaosDB.Exceptions.evaluate_return_code(err_code)
end


"""
    function set_name(parent::Ref{_Parent}, name::AbstractString)

Set the name of the given `parent` object.
"""
function set_name(parent::Ref{_Parent}, name::AbstractString)
    err_code = ccall(
        (:caosdb_entity_parent_set_name, CaosDB.library_name),
        Cint,
        (Ref{_Parent}, Cstring),
        parent,
        name,
    )

    CaosDB.Exceptions.evaluate_return_code(err_code)
end

"""
    function set_description(entity::Ref{_Entity}, description::AbstractString)

Set the description of the given `entity` object.
"""
function set_description(entity::Ref{_Entity}, description::AbstractString)
    err_code = ccall(
        (:caosdb_entity_entity_set_description, CaosDB.library_description),
        Cint,
        (Ref{_Entity}, Cstring),
        entity,
        description,
    )

    CaosDB.Exceptions.evaluate_return_code(err_code)
end

"""
    function set_datatype(
        entity::Ref{_Entity},
        datatype::AbstractString,
        collection::Union{CaosDB.Constants.COLLECTION._COLLECTION, Nothing}
    )

Set the datatype of the given `entity` object to a reference to the given
`datatype` name. Only possible if the role of the entity is "PROPERTY". Specify
whether the datatype is a list by specifying a `collection`. If none is given, a
scalar datatype is set.
"""
function set_datatype(
    entity::Ref{_Entity},
    datatype::AbstractString,
    collection::Union{CaosDB.Constants.COLLECTION._COLLECTION,Nothing} = nothing,
)

    if get_role(entity) != CaosDB.Constants.ROLE.PROPERTY
        throw(
            CaosDB.Exceptions.ClientException(
                "Only entities with role PROPERTY can be assigned a datatype.",
            ),
        )
    end

    if collection == nothing
        dtype_ref = create_reference_datatype(datatype)
    elseif collection == CaosDB.Constants.COLLECTION.LIST
        dtype_ref = create_reference_list_datatype(datatype)
    else
        throw(ArgumentError("Unkown datatype: $datatype"))
    end

    err_code = ccall(
        (:caosdb_entity_entity_set_datatype, CaosDB.library_name),
        Cint,
        (Ref{_Entity}, Ref{_DataType}),
        entity,
        dtype_ref,
    )

    CaosDB.Exceptions.evaluate_return_code(err_code)
end

"""
    function set_datatype(
        entity::Ref{_Entity},
        datatype::CaosDB.Constants.DATATYPE._DATATYPE,
        collection::Union{CaosDB.Constants.COLLECTION._COLLECTION, Nothing}
    )

Set the datatype of the given `entity` object to a reference to the given atomic
`datatype`. Only possible if the role of the entity is "PROPERTY". Specify
whether the datatype is a list by specifying a `collection`. If none is given, a
scalar datatype is set.
"""
function set_datatype(
    entity::Ref{_Entity},
    datatype::CaosDB.Constants.DATATYPE._DATATYPE,
    collection::Union{CaosDB.Constants.COLLECTION._COLLECTION,Nothing} = nothing,
)

    if get_role(entity) != CaosDB.Constants.ROLE.PROPERTY
        throw(
            CaosDB.Exceptions.ClientException(
                "Only entities with role PROPERTY can be assigned a datatype.",
            ),
        )
    end

    if collection == nothing
        dtype_ref = create_atomic_datatype(datatype)
    elseif collection == CaosDB.Constants.COLLECTION.LIST
        dtype_ref = create_atomic_list_datatype(datatype)
    else
        throw(ArgumentError("Unkown datatype: $datatype"))
    end

    err_code = ccall(
        (:caosdb_entity_entity_set_datatype, CaosDB.library_name),
        Cint,
        (Ref{_Entity}, Ref{_DataType}),
        entity,
        dtype_ref,
    )

    CaosDB.Exceptions.evaluate_return_code(err_code)
end

"""
    function set_datatype(entity::Ref{_Entity}, datatype::Tuple{Any, Any})

Convenience wrapper for the `set_datatype` functions s.th. expressions like
`set_datatype(entity_a, get_datatype(entity_b))` work.
"""
function set_datatype(entity::Ref{_Entity}, datatype::Tuple{Any,Any})
    set_datatype(entity, datatype[1], datatype[2])
end

"""
    function set_datatype(
        property::Ref{_Property},
        datatype::AbstractString,
        collection::Union{CaosDB.Constants.COLLECTION._COLLECTION, Nothing}
    )

Set the datatype of the given `property` object to a reference to the given
`datatype` name. Specify whether the datatype is a list by specifying a
`collection`. If none is given, a scalar datatype is set.
"""
function set_datatype(
    property::Ref{_Property},
    datatype::AbstractString,
    collection::Union{CaosDB.Constants.COLLECTION._COLLECTION,Nothing} = nothing,
)
    if collection == nothing
        dtype_ref = create_reference_datatype(datatype)
    elseif collection == CaosDB.Constants.COLLECTION.LIST
        dtype_ref = create_reference_list_datatype(datatype)
    else
        throw(ArgumentError("Unkown datatype: $datatype"))
    end

    err_code = ccall(
        (:caosdb_entity_property_set_datatype, CaosDB.library_name),
        Cint,
        (Ref{_Property}, Ref{_DataType}),
        property,
        dtype_ref,
    )

    CaosDB.Exceptions.evaluate_return_code(err_code)
end

"""
    function set_datatype(
        property::Ref{_Property},
        datatype::CaosDB.Constants.DATATYPE._DATATYPE,
        collection::Union{CaosDB.Constants.COLLECTION._COLLECTION, Nothing}
    )

Set the datatype of the given `property` object to a reference to the given
atomic `datatype`. Specify whether the datatype is a list by specifying a
`collection`. If none is given, a scalar datatype is set.
"""
function set_datatype(
    property::Ref{_Property},
    datatype::CaosDB.Constants.DATATYPE._DATATYPE,
    collection::Union{CaosDB.Constants.COLLECTION._COLLECTION,Nothing} = nothing,
)

    if collection == nothing
        dtype_ref = create_atomic_datatype(datatype)
    elseif collection == CaosDB.Constants.COLLECTION.LIST
        dtype_ref = create_atomic_list_datatype(datatype)
    else
        throw(ArgumentError("Unkown datatype: $datatype"))
    end

    err_code = ccall(
        (:caosdb_entity_property_set_datatype, CaosDB.library_name),
        Cint,
        (Ref{_Property}, Ref{_DataType}),
        property,
        dtype_ref,
    )

    CaosDB.Exceptions.evaluate_return_code(err_code)
end

"""
    function set_datatype(property::Ref{_Property}, datatype::Tuple{Any, Any})

Convenience wrapper for the `set_datatype` functions s.th. expressions like
`set_datatype(property_a, get_datatype(property_b))` work.
"""
function set_datatype(property::Ref{_Property}, datatype::Tuple{Any,Any})
    set_datatype(property, datatype[1], datatype[2])
end

"""
    function set_unit(entity::Ref{_Entity}, unit::AbstractString)

Set the unit of the given `entity` object.
"""
function set_unit(entity::Ref{_Entity}, unit::AbstractString)
    err_code = ccall(
        (:caosdb_entity_entity_set_unit, CaosDB.library_name),
        Cint,
        (Ref{_Entity}, Cstring),
        entity,
        unit,
    )

    CaosDB.Exceptions.evaluate_return_code(err_code)
end

"""
    function set_unit(property::Ref{_Property}, unit::AbstractString)

Set the unit of the given `property` object.
"""
function set_unit(property::Ref{_Property}, unit::AbstractString)
    err_code = ccall(
        (:caosdb_entity_property_set_unit, CaosDB.library_name),
        Cint,
        (Ref{_Property}, Cstring),
        property,
        unit,
    )

    CaosDB.Exceptions.evaluate_return_code(err_code)
end

"""
    function set_value(
        entity::Ref{_Entity},
        value::Union{AbstractString,Number,Bool,Vector{T}},
    ) where {T<:Union{AbstractString,Number,Bool}}


Set the value of the given `entity` object. Throw an error if it
doesn't have the correct role. The value must be either string,
Boolean, Integer, Float, or a vector thereof.
"""
function set_value(
    entity::Ref{_Entity},
    value::Union{AbstractString,Number,Bool,Vector{T}},
) where {T<:Union{AbstractString,Number,Bool}}

    if get_role(entity) != CaosDB.Constants.ROLE.PROPERTY
        throw(
            CaosDB.Exceptions.ClientException(
                "Only entites with role PROPERTY may be assigned a value.",
            ),
        )
    end

    value_ref = create_value(value)

    err_code = ccall(
        (:caosdb_entity_entity_set_value, CaosDB.library_name),
        Cint,
        (Ref{_Entity}, Ref{_Value}),
        entity,
        value_ref,
    )

    CaosDB.Exceptions.evaluate_return_code(err_code)
end


"""
    function set_value(property::Ref{_Property}, value::AbstractString)

Set the value of the given `property` object.
"""
function set_value(
    property::Ref{_Property},
    value::Union{AbstractString,Number,Bool,Vector{T}},
) where {T<:Union{AbstractString,Number,Bool}}

    value_ref = create_value(value)

    err_code = ccall(
        (:caosdb_entity_property_set_value, CaosDB.library_name),
        Cint,
        (Ref{_Property}, Ref{_Value}),
        property,
        value_ref,
    )

    CaosDB.Exceptions.evaluate_return_code(err_code)
end

"""
    function set_importance(
        property::Ref{_Property},
        importance::CaosDB.Constants.IMPORTANCE._IMPORTANCE
    )

Set the importance of the given `property` object.
"""
function set_importance(
    property::Ref{_Property},
    importance::CaosDB.Constants.IMPORTANCE._IMPORTANCE,
)
    err_code = ccall(
        (:caosdb_entity_property_set_importance, CaosDB.library_name),
        Cint,
        (Ref{_Property}, Cstring),
        property,
        string(importance),
    )

    CaosDB.Exceptions.evaluate_return_code(err_code)
end
"""
    function set_local_path(entity::Ref{_Entity}, path::AbstractString)

Set the local path of the given `entity` object.
"""
function set_local_path(entity::Ref{_Entity}, path::AbstractString)
    err_code = ccall(
        (:caosdb_entity_entity_set_local_path, CaosDB.library_name),
        Cint,
        (Ref{_Entity}, Cstring),
        entity,
        path,
    )

    CaosDB.Exceptions.evaluate_return_code(err_code)
end

"""
    function set_remote_path(entity::Ref{_Entity}, path::AbstractString)

Set the remote file path of the given `entity` object.
"""
function set_remote_path(entity::Ref{_Entity}, path::AbstractString)
    err_code = ccall(
        (:caosdb_entity_entity_set_file_path, CaosDB.library_name),
        Cint,
        (Ref{_Entity}, Cstring),
        entity,
        path,
    )

    CaosDB.Exceptions.evaluate_return_code(err_code)
end

"""
    function append_parent(entity::Ref{_Entity}, parent::Ref{_Parent})

Append the given `parent` to the parents of the given `entity`.
"""
function append_parent(entity::Ref{_Entity}, parent::Ref{_Parent})

    err_code = ccall(
        (:caosdb_entity_entity_append_parent, CaosDB.library_name),
        Cint,
        (Ref{_Entity}, Ref{_Parent}),
        entity,
        parent,
    )

    CaosDB.Exceptions.evaluate_return_code(err_code)
end

"""
    function append_parents(entity::Ref{_Entity}, parents::Vector{Base.RefValue{_Parent}})

Append all given `parents` to the parents of the given `entity`.
"""
function append_parents(entity::Ref{_Entity}, parents::Vector{Base.RefValue{_Parent}})

    for parent in parents
        append_parent(entity, parent)
    end
end

"""
    function remove_parent(entity::Ref{_Entity}, index::Cint)

Remove the parent at position `index` from the parents of the given
entity.
"""
function remove_parent(entity::Ref{_Entity}, index::Cint)

    err_code = ccall(
        (:caosdb_entity_entity_remove_parent, CaosDB.library_name),
        Cint,
        (Ref{_Entity}, Cint),
        entity,
        index - Cint(1),
    )

    CaosDB.Exceptions.evaluate_return_code(err_code)
end

"""
    function append_property(entity::Ref{_Entity}, property::Ref{_Property})

Append the given `property` to the properties of the given `entity`.
"""
function append_property(entity::Ref{_Entity}, property::Ref{_Property})

    err_code = ccall(
        (:caosdb_entity_entity_append_property, CaosDB.library_name),
        Cint,
        (Ref{_Entity}, Ref{_Property}),
        entity,
        property,
    )

    CaosDB.Exceptions.evaluate_return_code(err_code)
end

"""
    function append_properties(entity::Ref{_Entity}, properties::Vector{Base.RefValue{_Property}})

Append all given `properties` to the properties of the given `entity`.
"""
function append_properties(
    entity::Ref{_Entity},
    properties::Vector{Base.RefValue{_Property}},
)

    for property in properties
        append_property(entity, property)
    end
end

"""
    function remove_property(entity::Ref{_Entity}, index::Cint)

Remove the property at position `index` from the properties of the given
entity.
"""
function remove_property(entity::Ref{_Entity}, index::Cint)

    err_code = ccall(
        (:caosdb_entity_entity_remove_property, CaosDB.library_name),
        Cint,
        (Ref{_Entity}, Cint),
        entity,
        index - Cint(1),
    )

    CaosDB.Exceptions.evaluate_return_code(err_code)
end

"""
    function has_errors(entity::Ref{_Entity})

Return true if the given `entity` has errors, false otherwise.
"""
function has_errors(entity::Ref{_Entity})

    return get_errors_size(entity) > 0

end

"""
    function has_warnings(entity::Ref{_Entity})

Return true if the given `entity` has warnings, false otherwise.
"""
function has_warnings(entity::Ref{_Entity})

    return get_warnings_size(entity) > 0

end

end
