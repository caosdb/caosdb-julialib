using Documenter, CaosDB

makedocs(
    modules = [CaosDB],
    sitename = "CaosDB.jl",
    format = Documenter.HTML(prettyurls = false),
    pages = [
        "Home" => "index.md",
        "Getting started" => "README_SETUP.md",
        "examples.md",
        "Library" => "api.md",
    ],
    repo = "https://gitlab.com/caosdb/caosdb-julialib/blob/{commit}{path}#{line}",
)
