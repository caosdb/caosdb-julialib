# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2021 Indiscale GmbH <info@indiscale.com>
# Copyright (C) 2021 Florian Spreckelsen <f.spreckelsen@indiscale.com>
# Copyright (C) 2021 Alexander Kreft
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with this program. If not, see
# <https://www.gnu.org/licenses/>.
#
# ** end header
#

module CaosDB

# Exports from module Exceptions
export evaluate_return_code,
    CaosDBException, ClientException, GenericCaosDBException, CaosDBMessage

# Exports from module Constants
# ...from Constants namespace
export COLLECTION, DATATYPE, IMPORTANCE, MIN_CCAOSDB_VERSION, ROLE
# ...from CaosDB datatypes
export BOOLEAN, DATETIME, DOUBLE, INTEGER, TEXT
# ...from CaosDB collections
export LIST
# ...from CaosDB entity roles
export FILE, PROPERTY, RECORD, RECORD_TYPE
# ...from CaosDB importances
export FIX, OBLIGATORY, RECOMMENDED, SUGGESTED

# Exports from module Utility
export get_ccaosdb_version, get_env_fallback

# Export from module Connection
export connect, connect_manually

# Exports from module Entity
# Creators
export create_entity,
    create_parent,
    create_property,
    create_property_entity,
    create_record,
    create_recordtype,
    create_file_entity

# getters
export get_id,
    get_role,
    get_name,
    get_description,
    get_datatype,
    get_unit,
    get_value,
    get_version_id,
    get_property,
    get_properties,
    get_parent,
    get_parents,
    get_error,
    get_errors,
    get_warning,
    get_warnings,
    get_info,
    get_infos,
    get_importance,
    get_code

# setters
export append_parent,
    append_parents,
    append_property,
    append_properties,
    remove_parent,
    remove_property,
    set_id,
    set_role,
    set_name,
    set_description,
    set_datatype,
    set_unit,
    set_value,
    set_importance,
    set_local_path,
    set_remote_path

# helper functions
export has_errors, has_warnings

# Exports from module Transaction
export create_transaction,
    add_retrieve_by_id,
    add_insert_entity,
    add_update_entity,
    add_delete_by_id,
    add_retrieve_and_download_file_by_id,
    add_query,
    execute,
    get_result_set,
    get_count_result,
    get_result_at,
    get_results,
    execute_query,
    retrieve,
    insert_entity,
    update_entity,
    delete_by_id,
    retrieve_and_download_file_by_id

using Libdl

"""
Chose the name of the library according to the OS you're running.
"""
library_name = (@static Sys.iswindows() ? "ccaosdb" : "libccaosdb")
if isempty(find_library(library_name))
    @error "Could not find $library_name"
end

# include modules from other files

include("Exceptions.jl")

include("Constants.jl")

include("Info.jl")

include("Utility.jl")

include("Authentication.jl")

include("Connection.jl")

include("Entity.jl")

include("Transaction.jl")

using .Exceptions,
    .Constants, .Info, .Authentication, .Connection, .Utility, .Entity, .Transaction

end # CaosDB
