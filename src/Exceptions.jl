# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2021 Indiscale GmbH <info@indiscale.com>
# Copyright (C) 2021 Florian Spreckelsen <f.spreckelsen@indiscale.com>
# Copyright (C) 2021 Alexander Kreft
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with this program. If not, see
# <https://www.gnu.org/licenses/>.
#
# ** end header
#
module Exceptions

export evaluate_return_code,
    CaosDBException, ClientException, GenericCaosDBException, CaosDBMessage

using Logging
using CaosDB

"""
The parent type of all CaosDB errors that can also be used for testing.
"""
abstract type CaosDBException <: Exception end

"""
A generic exception that will be raised in case of non-zero return
values of the calls to libccaosdb. May carry a message string and a
code.
"""
struct GenericCaosDBException <: CaosDBException
    msg::String
    code::Cint
end

"""
Something went wrong on the client-side or the user is attempting to
conduct an invalid operation.
"""
struct ClientException <: CaosDBException
    msg::String
    code::Cint

    function ClientException(message::AbstractString)
        client_error_code =
            ccall((:caosdb_status_code_OTHER_CLIENT_ERROR, CaosDB.library_name), Cint, ())
        new(message, client_error_code)
    end
end


Base.showerror(io::IO, e::CaosDBException) =
    print(io, "CaosDBException: ", e.msg, " (Status code ", e.code, ")")

"""
Struct containing Messages and codes for status codes<0 that do not
correspond to errors or success.
"""
struct CaosDBMessage
    msg::String
    code::Cint
end

Base.show(io::IO, m::CaosDBMessage) = print(io, m.msg, " (Status code ", m.code, ")")

"""
    function evaluate_return_code(code::Cint)

Evaluate the return code of a libccaosdb ccall and raise a
`GenericCaosDBException` in case of a non-zero return code.
"""
function evaluate_return_code(code::Cint)
    if code != 0
        msg = ccall(
            (:caosdb_get_status_description, CaosDB.library_name),
            Cstring,
            (Cint,),
            code,
        )
        if code > 0
            throw(GenericCaosDBException(unsafe_string(msg), code))
        else
            @info CaosDBMessage(unsafe_string(msg), code)
        end
    end
end

end # Exceptions
