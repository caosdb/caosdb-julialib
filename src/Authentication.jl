# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2021 Indiscale GmbH <info@indiscale.com>
# Copyright (C) 2021 Florian Spreckelsen <f.spreckelsen@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with this program. If not, see
# <https://www.gnu.org/licenses/>.
#
# ** end header
#
module Authentication

using ..CaosDB

"""
Struct containing a pointer to the wrapped cpp authenticator
class. Meant for internal use; call a
`CaosDB.Authentication.create_<authenticator>` function to create an
authenticator object from a configuration.
"""
mutable struct _Authenticator
    wrapped_authenticator::Ptr{Cvoid}
    _deletable::Bool

    function _Authenticator(managed_by_julia::Bool = false)
        auth = new()
        auth._deletable = false
        if managed_by_julia
            # Only append a finalizer for this if the object is
            # actually managed by Julia and not created and destroyed
            # internally by libcaosdb.
            function f(t)
                ccall(
                    (:caosdb_authentication_delete_authenticator, CaosDB.library_name),
                    Cint,
                    (Ref{_Authenticator},),
                    Ref{_Authenticator}(t),
                )
            end
            finalizer(f, auth)
        end
        return auth
    end
end

"""
    create_plain_password_authenticator(
        username::AbstractString,
        password::AbstractString,
    )

Return an authenticator object that contains a wrapped cpp
plain-password authenticator configured with `username` and
`password`.
"""
function create_plain_password_authenticator(
    username::AbstractString,
    password::AbstractString,
)

    auth = Ref{_Authenticator}(_Authenticator(true))

    err_code = ccall(
        (:caosdb_authentication_create_plain_password_authenticator, CaosDB.library_name),
        Cint,
        (Ref{_Authenticator}, Cstring, Cstring),
        auth,
        username,
        password,
    )

    CaosDB.Exceptions.evaluate_return_code(err_code)

    return auth

end

end # Authentication
