# CaosDB.jl's API

Below you find the explicit documentation of CaosDB's public
interface. You also find the documentation of the internal API which
is meant for expert use only. Only use those if you know what you're
doing.

```@index
Order = [:module, :function, :macro, :type, :constant]
```

## Public API

```@autodocs
Modules=[CaosDB, CaosDB.Exceptions, CaosDB.Constants, CaosDB.Info, CaosDB.Utility,
	CaosDB.Connection, CaosDB.Authentication, CaosDB.Entity,
	CaosDB.Transaction]
Private=false
```

## Expert-use only API functions

```@autodocs
Modules=[CaosDB, CaosDB.Exceptions, CaosDB.Constants, CaosDB.Info, CaosDB.Utility,
	CaosDB.Connection, CaosDB.Authentication, CaosDB.Entity,
	CaosDB.Transaction]
Public=false
```
