# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

* Basic functionality to establish connection to a CaosDB server and
  retrieve its version (using the Extern C interface of caosdb-cpplib)
* Support for Windows
* Support for config files
* Minimal error handling
* Queries
* Retrieval by id(s)
* Enums for CaosDB datatypes, importances, roles, and collections

### Changed

* Structs for value and datatype are now used internally to set and get values
  of entities and properties.
* `get_datatype` now returns a tuple with the datatype name and the collection
  type (or nothing in case of scalar datatypes)

### Deprecated

### Removed

### Fixed

* `get_value` functions for properties and entities
* `retrieve` function (no SegFaults anymore)

### Security
